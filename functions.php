<?php 
session_start();


// connect to database
$db = mysqli_connect('localhost', 'root', '', 'userstest') or  die ('Connection_Error');

// variable declaration
$username = "";
$email    = "";
$errors   = array(); 
$pid      = "";
$name      = "";
$designation = "";
$id="";

// call the register() function if register_btn is clicked
if (isset($_POST['register_btn'])) {
	register();
}

// REGISTER USER
function register(){
	// call these variables with the global keyword to make them available in function
	global $db, $errors, $username, $email;

	// receive all input values from the form. Call the e() function
    // defined below to escape form values
	$username    =  $_POST['username'];
	$email       =  $_POST['email'];
	$password_1  =  $_POST['password_1'];
    $password_2  =  $_POST['password_2'];
	$usertype    =  $_POST['usertype'];
	$file =  $_FILES['file'];
    $errors   = array(); 

	// form validation: ensure that the form is correctly filled
	if (empty($username)) { 
		array_push($errors, "Username is required"); 
	}
	if (empty($email)) { 
		array_push($errors, "Email is required"); 
	}
	if (empty($password_1)) { 
		array_push($errors, "Password is required"); 
	}
	if (empty($file)) { 
		array_push($errors, "Password is required"); 
	}
	if ($password_1 != $password_2) {
		array_push($errors, "The two passwords do not match");
	}

	// register user if there are no errors in the form
	  if (count($errors) == 0) {
		$filename = $file['name'];
		$fileerror= $file['error'];
		$filetmp = $file['tmp_name']; 
		$fileext= explode('.' , $filename);
		$filecheck = strtolower(end($fileext));
		$fileextstored = array('png', 'jpg', 'jpeg');
		$password = md5($password_1);//encrypt the password before saving in the database

		 if (isset($_POST['usertype'])) {
			$usertype = $_POST['usertype'];
			if (in_array($filecheck, $fileextstored)) {
				$destinationfile = 'img/'.$filename;
				move_uploaded_file($filetmp,$destinationfile);
			$query = "INSERT INTO users (username, email,  password, usertype , image) 
					  VALUES('$username', '$email', '$password', '$usertype', '$destinationfile' )";
			mysqli_query($db, $query) or  die ('Insertion_Error');
			
		
			$_SESSION['success']  = "New user successfully created!!";
			$_SESSION['msg_type']  = "success";
			header('location: login.php');
			}else  { echo "File Extension does't match"; }
		}else{
			$query = "INSERT INTO users (username, email,  password, usertype, image) 
            VALUES('$username', '$email', '$password', '$usertype', '$destinationfile' )";
			mysqli_query($db, $query);

			// get id of the created user
			$logged_in_user_id = mysqli_insert_id($db);

			$_SESSION['user'] = getUserById($logged_in_user_id); // put logged in user in session
			$_SESSION['success']  = "You are successfully registered ";
			$_SESSION['msg_type']  = "success";

			
			header('location: index.php');				
		}
	}
}
// return user array from their id
function getUserById($id){
	global $db;
	$query = "SELECT * FROM users WHERE id=" . $id;
	$result = mysqli_query($db, $query);

	$user = mysqli_fetch_assoc($result);
	return $user;
}

// escape string
function e($val){
	global $db;
	return mysqli_real_escape_string($db, trim($val));
}

function display_error() {
	global $errors;

	if (count($errors) > 0){
		echo '<div class="error">';
			foreach ($errors as $error){
				echo $error .'<br>';
			}
		echo '</div>';
	}
}	
function isLoggedIn()
{
	if (isset($_SESSION['user'])) {
		return true;
	}else{
		return false;
	}
}
if (isset($_GET['logout'])) {
	session_destroy();
	unset($_SESSION['user']);
	header("location: main.php");
}
// call the login() function if register_btn is clicked
if (isset($_POST['login_btn'])) {
	login();
}

// LOGIN USER
function login(){
	global $db, $username, $errors;

	// grap form values
	$username = e($_POST['username']);
	$password = e($_POST['password']);

	// make sure form is filled properly
	if (empty($username)) {
		array_push($errors, "Username is required");
	}
	if (empty($password)) {
		array_push($errors, "Password is required");
	}

	// attempt login if no errors on form
	if (count($errors) == 0) {
		$password = md5($password);

		$query = "SELECT * FROM users WHERE username='$username' AND password='$password' LIMIT 1";
		$results = mysqli_query($db, $query);

		if (mysqli_num_rows($results) == 1) { // user found
			// check if user is admin or user
			$logged_in_user = mysqli_fetch_assoc($results);
			if ($logged_in_user['usertype'] == 'admin') {

				$_SESSION['user'] = $logged_in_user;
				header('location: home.php');		  
			}else{
				$_SESSION['user'] = $logged_in_user;
				header('location: index.php');
			}
			
		}else {
			
			$_SESSION['success']  = "Wrong username/password combination";
			$_SESSION['msg_type']  = "danger";
			
		}
	}
}
function isAdmin()
{
	if (isset($_SESSION['user']) && $_SESSION['user']['usertype'] == 'admin' ) {
		return true;
	}else{
		return false;
	}
}

if (isset($_POST['save'])) {
	saveData();

}
   function saveData(){

	global $db, $pid, $name, $designation;
	// initialize variables
	$pid = $_POST['pid'];
	$name = $_POST['name'];
	$designation = $_POST['designation'];
	
	$update = false;
	if (empty($pid)) { 
		array_push($errors, "pid is required"); 
	}
	if (empty($name)) { 
		array_push($errors, "Name is required"); 
	}
	if (empty($designation)) { 
		array_push($errors, "Designation is required"); 
	}

	
		$query = "INSERT INTO employees(pid,name,designation) VALUES ('$pid','$name', '$designation')";
	
		mysqli_query($db, $query ) or  die ('Insertion_Error'); 
		$_SESSION['success'] = "your attendance has been saved"; 
		$_SESSION['msg_type'] = "success"; 
		header('location: index.php');
	}
	if (isset($_GET['delete'])) {
		deleteData();
	
	}
	function deleteData(){
        global $db , $id;
		$id= $_GET['delete'];
		
		$query = "DELETE FROM employees WHERE id= $id ";
		mysqli_query($db, $query ) or  die ('Delete_Error'); 
		$_SESSION['success'] = "Record has been deleted"; 
		$_SESSION['msg_type'] = "danger"; 
		header('location: home.php');
	}

	if (isset($_GET['edit'])) {
		editData();
	
	}
	function editData(){
		global $db,$id,$pid,$name,$designation;
		
		$id= $_GET['edit'];
		
		$query = " SELECT * FROM employees WHERE id=$id";
		$result = mysqli_query($db, $query); 

		if (count($result)==1){
			
		$row = mysqli_fetch_array($result);
		$id = $row['id'];
		$pid = $row['pid'];	
		$name = $row['name'];	
		$designation = $row['designation'];	
		header('location');
		}
	}
	if (isset($_POST['update'])) {

		updateData();
	}
	function updateData(){
		global $db,$id,$pid,$name,$designation;

		$id= $_POST['id'];
		$pid= $_POST['pid'];
		$name= $_POST['name'];
		$designation = $_POST['designation'];
		

        $query = "UPDATE employees SET pid='$pid', name = '$name', designation= '$designation' WHERE id= $id ";
		mysqli_query($db, $query ) or  die ('Update Error'); 
		$_SESSION['success'] = "Record has been updates successfully"; 
		$_SESSION['msg_type'] = "warning"; 
        header('location: home.php ');
		
	
	}

	
    
?>