<?php 
    include('functions.php');
    if (!isLoggedIn()) {
	$_SESSION['msg'] = "You must log in first";
	header('location: login.php');
}
if (isset($_GET['logout'])) {
	session_destroy();
	unset($_SESSION['user']);
	header("location: main.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Attendance Sheet</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
<?php include('includes/header.php');?>
<?php include('includes/menubar.php');?>
<!-- notification message -->
<?php if (isset($_SESSION['success'])): ?> 
	<div class="alert alert-<?=$_SESSION['msg_type']?>">
	<?php
	echo $_SESSION['success'];
	unset($_SESSION['success']);
     ?>
	
	</div>
<?php endif; ?>

<div class="wrapper">
	<div class="header">
		<h2>Attendance Sheet</h2>
        <p> Plesae Mark your attendance</p>
	</div>
	<div class="form-group">
		<!-- notification message -->
		<?php if (isset($_SESSION['success'])) : ?>
			<div class="form-group" >
				<h3>
					<?php 
						echo $_SESSION['success']; 
						unset($_SESSION['success']);
					?>
				</h3>
			</div>
		<?php endif ?>
		<!-- logged in user information -->
		<div class="form-group">
		<img src="<?php echo $_SESSION['user']['image']; ?>"
			  height = "100px" width="100px">
        </div>
			<div class="form-group">
				<?php  if (isset($_SESSION['user'])) : ?>
					<strong><?php echo $_SESSION['user']['username']; ?></strong>
                    <small>
						<i  style="color: #888;">(<?php echo ucfirst($_SESSION['user']['usertype']); ?>)</i> 
						<br>
						<a href="index.php?logout='1'" style="color: red;"><button type="logout" name="logout"  class="btn btn-default">Log out</button></a>
					</small>
				<?php endif ?>
			</div>
		
      
	</div>
    <form method="post" action="index.php" >
		<div class="form-group">
			<label>pid</label>
			<input type="text" name="pid" value="" class="form-control">
		</div>
		<div class="form-group">
			<label>Name</label>
			<input type="text" name="name" value="" class="form-control">
		</div>
        <div class="form-group">
			<label>Designation</label>
			<input type="text" name="designation" value="" class="form-control">
		</div>
		<div class="form-group">
			<button  type="submit" name="save"  class="btn btn-primary">Submit</button>
		</div>
		
	</form>
</div>

</body>
</html>