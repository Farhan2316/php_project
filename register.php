
<?php
include('functions.php'); ?>
<!DOCTYPE html>

<head>
	<title>Registration</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    
    <style type="text/css">
        body{ font: 14px sans-serif; }
       
    </style>
</head>
<body>

<?php include('includes/header.php');?>
<?php include('includes/menubar.php');?>
  <!-- notification message -->
<?php if (isset($_SESSION['success'])): ?> 
	<div class="alert alert-<?=$_SESSION['msg_type']?>">
	<?php
	echo $_SESSION['success'];
	unset($_SESSION['success']);
     ?>
	
	</div>
<?php endif; ?>

<div class="container">
<div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Sign up</h4>
	
    <p>Please fill this form to create an account.</p>
</div>
</div>



<form method="post" action="functions.php" enctype= "multipart/form-data">
<div class="row">
     <div class="col-md-6">
   
		<label>Username</label>
		<input type="text" name="username"  class="form-control">
	
         <br>
	
		<label>Email</label>
		<input type="email" name="email" class="form-control">
	   <br>
		<label>Password</label>
		<input type="password" name="password_1" class="form-control">

   <br>
	
		<label>Confirm password</label>
		<input type="password" name="password_2" class="form-control">
		<br>

    <label for="user">Choose a user:</label>

    <select name="usertype" id="usertype" class="form-control">
				
		<option value="admin">Admin</option>
		<option value="user">User</option>
	</select>
    <br>
		<label>Profile Picture:</label>
		<input type="file" name="file" id= "file" class="form-control">
		<br>
	

		<button type="submit" class="btn btn-success" name="register_btn" >Register</button>
        <button type="reset" class="btn btn-primary" name="cancel_btn" onClick="window.location.href='http://localhost/project/main.php';">Cancel</button>
	</div>
	
	

  </form>

</div>
<br>
<div class="row">
     <div class="col-md-12">
   
	<p>Already a member? <a href="login.php">Sign in</a></p>
   </div>
</div>
    

	<?php echo display_error(); ?>
</div>



 <!-- CONTENT-WRAPPER SECTION END-->
 <?php include('includes/footer.php');?>

</body>
</html>