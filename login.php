<?php include('functions.php')?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration system PHP and MySQL</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
<?php include('includes/header.php');?>
<?php include('includes/menubar.php');?>
 <!-- notification message -->
 <?php if (isset($_SESSION['success'])): ?> 
	<div class="alert alert-<?=$_SESSION['msg_type']?>">
	<?php
	echo $_SESSION['success'];
	unset($_SESSION['success']);
     ?>
	
	</div>
 <?php endif; ?>

	<?php echo display_error(); ?>	
    
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Sign In </h4>

                </div>

            </div>
             
            <form name="admin" method="post">
            <div class="row">
                <div class="col-md-6">
				<label>Username :</label>
			    <input type="text" name="username" class="form-control" />
	            <br>
			    <label>Password :</label>
				<input type="password" name="password" class="form-control" />
				
				
                 <hr />
                <button type="submit" name="login_btn" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> &nbsp;Login</button>&nbsp;
                </div>
				</form>
				
			</div>
			<br>
			<div class="row">
                <div class="col-md-12">
				<p>Not yet a member? <a href="register.php">Sign up</a> </p>
			 </div>
			 </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <?php include('includes/footer.php');?>
</body>
</html>