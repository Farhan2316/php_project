<?php 
include('functions.php');

if (!isAdmin()) {
	$_SESSION['msg'] = "You must login first";
	header('location: login.php');
}

if (isset($_GET['logout'])) {
	session_destroy();
	unset($_SESSION['user']);
	header("location: main.php");
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <style type="text/css">
        body{ font: 14px sans-serif; }
        
    </style>
	</style>
</head>
<body>
<?php include('includes/header.php');?>
<?php include('includes/menubar.php');?>
<!-- notification message -->
<?php if (isset($_SESSION['success'])): ?> 
	<div class="alert alert-<?=$_SESSION['msg_type']?>">
	<?php
	echo $_SESSION['success'];
	unset($_SESSION['success']);
     ?>
	
	</div>
<?php endif; ?>

<div class="container">
	<div class="row">
	<div class="col-6">
		<h2>Admin - Home Page</h2>
	</div>
	</div>
	<div class="row">
	<div class="col-sm-6" >
	
		<!-- logged in user information -->
	 <div class="profile_info" >
		<img src="<?php echo $_SESSION['user']['image']; ?>"
			  height = "100px" width="100px">

			<div>
				<?php  if (isset($_SESSION['user'])) : ?>
					<strong><?php echo $_SESSION['user']['username']; ?></strong>

					<small>
						<i  style="color: #888;">(<?php echo ucfirst($_SESSION['user']['usertype']); ?>)</i> 
						<br>
						<br>
						<div>
						<a href="home.php?logout='1'" style="color: red;"><button type="logout" name="logout"  class="btn btn-default">Log out</button></a>
                    <br>
					<br>
						</div>
					</small>

				<?php endif; ?>
				
			</div>
		</div>
	</div>

	

		<!-- get user information -->
		<div class="row">
	    <div class="col-12" >
	
		<?php
       $query = "SELECT * FROM employees";
		$results = mysqli_query($db, $query);
          ?>

		<div class="row justify-content-center">
		   <table class="table table-striped table-hover table-bordered">
		   <thead>
		        <tr>
		            <th>ID</th>
					<th>PID</th>
					<th>Name</th>
					<th>Designation</th>	
					<th>Edit</th>
					<th>Delete</th>

		        </tr>
		   </thead>

		   <?php while ($row=mysqli_fetch_array($results)){ ?>
		      <tr>
			  <td><?php echo $row['id']; ?></td>
			  <td><?php echo $row['pid']; ?></td>
			  <td><?php echo $row['name']; ?></td>
			  <td><?php echo $row['designation']; ?></td>
			  <td>
			   <a href="home.php?edit=<?php echo $row['id']; ?>" 
			   class = "btn btn-info">Edit</a>
			   </td>
			   <td>
			   <a href="functions.php?delete=<?php echo $row['id']; ?>" 
			   class = "btn btn-danger">Delete</a>
			  </td>
			  </tr>
		   <?php } ?>
		   </table>
		</div> 
		</div>
		</div>

     <?php if (isset($_GET['edit'])) { ?>	
		
    <form method="POST" action="home.php" >
	<div class="form-group">
		<label>id</label>
		<input type="text" name="id" value="<?php echo $id; ?>" class="form-control">
	</div>
	<div class="form-group">
		<label>pid</label>
		<input type="text" name="pid" value="<?php echo $pid; ?>" class="form-control">
	</div>
	<div class="form-group">
		<label>Name</label>
		<input type="text" name="name" value="<?php echo $name; ?>" class="form-control">
	</div>
	<div class="form-group">
		<label>Designation</label>
		<input type="text" name="designation" value="<?php echo $designation; ?>" class="form-control">
	</div>
	<div class="form-group">	
		<button  type="submit" name="update" class="btn btn-info">Update</button>
	
	</div>
	
     </form>

	 <?php } ?>
	

  
</body>
</html>